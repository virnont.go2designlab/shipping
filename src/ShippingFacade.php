<?php

namespace Go2designlab\Shipping;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Go2designlab\Shipping\Skeleton\SkeletonClass
 */
class ShippingFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'shipping';
    }
}
